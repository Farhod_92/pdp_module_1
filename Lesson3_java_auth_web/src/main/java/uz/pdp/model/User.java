package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
    private String username;
    private String password;
    private String firstName;

    public User(String username, String firstName) {
        this.username = username;
        this.firstName = firstName;
    }
}
