package uz.pdp.service;

import uz.pdp.model.Result;
import uz.pdp.model.User;

import java.sql.*;

public class DbService {
    String url = "jdbc:postgresql://localhost:5432/auth";
    String username = "postgres";
    String password = "123";

    public Result registerUser(User user){
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            String checkUsernameQuery = "select count(*) from users where username='" + user.getUsername() + "'";
            ResultSet resultSet = statement.executeQuery(checkUsernameQuery);

            int count = 0;
            while(resultSet.next()){
               count = resultSet.getInt(1);
            }

            if(count > 0)
                return new Result("Username exists", false);


            String addUserQuery = "insert into users(username, password, first_name) values(?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(addUserQuery);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFirstName());
            boolean execute = preparedStatement.execute();

            statement.close();
            preparedStatement.close();
            connection.close();

            return new Result("user added", true);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return new Result(throwables.getMessage(), false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return new Result(e.getMessage(), false);
        }
    }

    public User login(String username, String password){
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
            String query = "select * from users where username=? and password=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                User user = new User(resultSet.getString(2), resultSet.getString(3));
                preparedStatement.close();
                connection.close();
                return user;
            }
            preparedStatement.close();
            connection.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }
    public User loadUserByCookie(String username){
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
            String query = "select * from users where username=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                User user = new User(resultSet.getString(1), resultSet.getString(3));
                preparedStatement.close();
                connection.close();
                return user;
            }
            preparedStatement.close();
            connection.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }
}
