package uz.pdp.controller;

import uz.pdp.model.Result;
import uz.pdp.model.User;
import uz.pdp.service.DbService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Register extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("register.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String prePassword = req.getParameter("prePassword");
        String firstName = req.getParameter("firstName");

        PrintWriter writer = resp.getWriter();

        if(password.equals(prePassword)){
            DbService dbService =new DbService();
            Result result = dbService.registerUser(new User(username, password, firstName));

            if(result.isSuccess())
                writer.write("<h1 style = 'color:green'>"+result.getMessage()+" </h1>");
            else
                writer.write("<h1 style = 'color:red'>"+result.getMessage()+" </h1>");
        }else
                writer.write("<h1 style = 'color:red'> Passwordlar to'g'ri kelmadi! </h1>");
    }
}
