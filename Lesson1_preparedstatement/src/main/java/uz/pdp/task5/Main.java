package uz.pdp.task5;

import uz.pdp.task1.Task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DBService dbService = new DBService();

        float minPrice  = -1;
        float maxPrice  = -1;

        while ((minPrice != 0 || maxPrice != 0)) {
            System.out.println("0=>Exit, Enter minPrice");
            minPrice = scanner.nextFloat();
            System.out.println("0=>Exit, Enter maxPrice");
            maxPrice = scanner.nextFloat();

            dbService.getProductsBetweenPrices(minPrice, maxPrice);


        }
    }
}
