package uz.pdp.task5;

import uz.pdp.task3.PC;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBService {    private String url = "jdbc:postgresql://localhost:5432/computer_db";
    private String username = "postgres";
    private String password = "123";



    public void getProductsBetweenPrices(float minPrice, float maxPrice){
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            String pcs = "{call get_products_by_price(?,?)}";

            CallableStatement callableStatement = connection.prepareCall(pcs);
            callableStatement.setFloat(1, minPrice);
            callableStatement.setFloat(2, maxPrice);
           // callableStatement.registerOutParameter(3, Types.OTHER);
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();

            List<Product> productList = new ArrayList<Product>();
            while (resultSet.next()){
                productList.add(new Product(resultSet.getString(1), resultSet.getString(2), resultSet.getFloat(3)));
            }

            System.out.println("PRODUCT between " + minPrice + "$ and " + maxPrice + "$ : ");
            System.out.println("maker:   model:   price: ");
            for (Product pr : productList) {
                System.out.println(pr.getMaker() +"  \t \t"+ pr.getModel() +"\t \t"+ pr.getPrice());
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
