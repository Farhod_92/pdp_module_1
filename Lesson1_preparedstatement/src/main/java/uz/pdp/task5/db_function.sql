CREATE OR REPLACE FUNCTION public.get_products_by_price ( IN min_price numeric, IN max_price numeric)
    RETURNS TABLE (
		maker varchar,
		model varchar,
		price numeric
	)
    LANGUAGE 'plpgsql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100

AS $BODY$

begin
	return query
		select * from  (select p.maker, p.model, pc.price from product p join pc  on p.model = pc.model
		UNION select p.maker, p.model, l.price from product p join laptop l on p.model = l.model
		UNION select p.maker, p.model, pr.price from product p join printer pr on p.model = pr.model
		order by price) as all_products where all_products.price between min_price  and max_price;

end;
$BODY$;