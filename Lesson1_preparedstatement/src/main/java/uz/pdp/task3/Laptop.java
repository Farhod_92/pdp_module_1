package uz.pdp.task3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Laptop {
    private String maker;
    private Integer code;
    private String model;
    private short speed;
    private short ram;
    private float hd;
    private Integer price;
    private short screen;
}
