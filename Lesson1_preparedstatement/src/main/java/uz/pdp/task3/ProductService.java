package uz.pdp.task3;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductService {
    private String url = "jdbc:postgresql://localhost:5432/computer_db";
    private String username = "postgres";
    private String password = "123";

    public void printPCs(){
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            String pcs = "select * from product p join pc  on p.model = pc.model order by price asc";
            List<PC> pcList =new ArrayList<PC>();

            PreparedStatement preparedStatement = connection.prepareStatement(pcs);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                pcList.add(new PC(
                        resultSet.getString(1),
                        resultSet.getInt(4),
                        resultSet.getString(5),
                        resultSet.getShort(6),
                        resultSet.getShort(7),
                        resultSet.getFloat(8),
                        resultSet.getString(9),
                        resultSet.getInt(10)
                        ));
            }

            System.out.println("PCS");
            System.out.println("maker:   model:   price: ");
            for (PC pc : pcList) {
                System.out.println(pc.getMaker() +"  \t \t"+ pc.getModel() +"\t \t"+ pc.getPrice());
            }

            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void printLaptops(){
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            String laptops = "select * from product p join laptop l on p.model = l.model order by price asc";
            List<Laptop> laptopList = new ArrayList<Laptop>();

            PreparedStatement preparedStatement = connection.prepareStatement(laptops);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                laptopList.add(new Laptop(
                        resultSet.getString(1),
                        resultSet.getInt(4),
                        resultSet.getString(5),
                        resultSet.getShort(6),
                        resultSet.getShort(7),
                        resultSet.getFloat(8),
                        resultSet.getInt(9),
                        resultSet.getShort(10)
                ));
            }

            System.out.println("LAPTOPS");
            System.out.println("maker: model: price");
            for (Laptop laptop : laptopList) {
                System.out.println(laptop.getMaker() +"  \t \t"+ laptop.getModel() +"\t"+ laptop.getPrice());
            }

            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void printPrinters(){
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            String printers = "select * from product p join printer pr on p.model = pr.model order by price asc";
            List<Printer> printerList = new ArrayList<Printer>();

            PreparedStatement preparedStatement = connection.prepareStatement(printers);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                printerList.add(new Printer(
                        resultSet.getString(1),
                        resultSet.getInt(4),
                        resultSet.getString(5),
                        resultSet.getString(6).charAt(0),
                        resultSet.getString(7),
                        resultSet.getInt(8)
                ));
            }
            System.out.println("PRINTERS");
            System.out.println("maker:   model:   price: ");
            for (Printer printer : printerList) {
                System.out.println(printer.getMaker() +"  \t \t"+ printer.getModel() +"\t"+ printer.getPrice());
            }

            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
