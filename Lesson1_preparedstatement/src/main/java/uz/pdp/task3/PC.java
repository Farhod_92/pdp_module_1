package uz.pdp.task3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PC {
    private String maker;
    private Integer code;
    private String model;
    private short speed;
    private short ram;
    private float hd;
    private String cd;
    private Integer price;
}
