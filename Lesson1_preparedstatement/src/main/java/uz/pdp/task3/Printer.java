package uz.pdp.task3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Printer {
    private String maker;
    private Integer code;
    private String model;
    private char color;
    private String type;
    private Integer price;

}
