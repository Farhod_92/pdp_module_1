package uz.pdp.task3;

import uz.pdp.task1.Task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Task1 task1 = new Task1();

        int i = -1;
        ProductService productService = new ProductService();

        while ((i != 0)) {
            System.out.println("0=>Exit, 1=>PC, 2=>LAPTOP, 3=>PRINTER");
            i = scanner.nextInt();
            scanner = new Scanner(System.in);

            switch (i){
                case 1:
                    productService.printPCs();
                    break;
                case 2:
                    productService.printLaptops();
                    break;
                case 3:
                    productService.printPrinters();
                    break;
            }
        }
    }
}
