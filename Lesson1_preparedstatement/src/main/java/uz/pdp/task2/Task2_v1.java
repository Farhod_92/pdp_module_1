package uz.pdp.task2;

import java.sql.*;

public class Task2_v1 {

    public static void main(String[] args) {
        getAllProducts();
    }

    private static String url = "jdbc:postgresql://localhost:5432/computer_db";
    private static String username = "postgres";
    private static String password = "123";

    public static void getAllProducts(){
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            String laptops = "select maker, p.model, price from product p join laptop l on p.model = l.model order by price asc";
            String pcs = "select maker, p.model, price from product p join pc  on p.model = pc.model order by price asc";
            String printers = "select maker, p.model, price from product p join printer pr on p.model = pr.model order by price asc";

            PreparedStatement preparedStatement = connection.prepareStatement(laptops);
            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println("LAPTOPS");
            System.out.println("maker: model: price");
            while (resultSet.next()){
                System.out.print(resultSet.getString(1) + " : ");
                System.out.print(resultSet.getString(2) + " : ");
                System.out.print(resultSet.getString(3) + "\n");
            }

            preparedStatement = connection.prepareStatement(pcs);
            resultSet = preparedStatement.executeQuery();
            System.out.println("PCS");
            System.out.println("maker:   model:   price: ");
            while (resultSet.next()){
                System.out.print(resultSet.getString(1) + " : ");
                System.out.print(resultSet.getString(2) + " : ");
                System.out.print(resultSet.getString(3) + "\n");
            }

            preparedStatement = connection.prepareStatement(printers);
            resultSet = preparedStatement.executeQuery();
            System.out.println("PRINTERS");
            System.out.println("maker:   model:   price: ");
            while (resultSet.next()){
                System.out.print(resultSet.getString(1) + " : ");
                System.out.print(resultSet.getString(2) + " : ");
                System.out.print(resultSet.getString(3) + "\n");
            }

            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
