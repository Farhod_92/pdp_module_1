package uz.pdp.task2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Task2_v2 {

    public static void main(String[] args) {
        getAllProducts();
    }

    private static String url = "jdbc:postgresql://localhost:5432/computer_db";
    private static String username = "postgres";
    private static String password = "123";

    public static void getAllProducts(){
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            List<Product> productList=new ArrayList<Product>();

            String products = "select maker, p.model, price from product p join pc  on p.model = pc.model " +
                    "UNION " +
                    "select maker, p.model, price from product p join laptop l on p.model = l.model " +
                    "UNION " +
                    "select maker, p.model, price from product p join printer pr on p.model = pr.model " +
                    "order by price";

            PreparedStatement preparedStatement = connection.prepareStatement(products);
            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println("PRODUCTS");
            System.out.println("maker: model: price");
            while (resultSet.next()){
                System.out.print(resultSet.getString(1) + " : ");
                System.out.print(resultSet.getString(2) + " : ");
                System.out.print(resultSet.getFloat(3) + "\n");
                productList.add(new Product(resultSet.getString(1), resultSet.getString(2), resultSet.getFloat(3)));
            }


            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
