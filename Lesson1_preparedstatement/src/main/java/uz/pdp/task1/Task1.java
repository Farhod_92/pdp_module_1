package uz.pdp.task1;

import java.sql.*;

public class Task1 {
    private String url = "jdbc:postgresql://localhost:5432/computer_db";
    private String username = "postgres";
    private String password = "123";

    public void getAllProducts(){
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            String query = "select * from product";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println("user list");
            while (resultSet.next()){
                System.out.print(resultSet.getString(1) + " : ");
                System.out.print(resultSet.getString(2) + " : ");
                System.out.print(resultSet.getString(3) + "\n");
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
