package uz.pdp.task4_callablestatement;

import java.sql.*;

public class ProductService {
    private String url = "jdbc:postgresql://localhost:5432/computer_db";
    private String username = "postgres";
    private String password = "123";

    public boolean addProduct(Product product){
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            String pcs = "{call add_product(?,?,?,?)}";

            CallableStatement callableStatement = connection.prepareCall(pcs);
            callableStatement.setString(1, product.getMaker());
            callableStatement.setString(2, product.getModel());
            callableStatement.setString(3, product.getType());
            callableStatement.registerOutParameter(4, Types.BOOLEAN);
            callableStatement.execute();
            return callableStatement.getBoolean(4);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }
}
