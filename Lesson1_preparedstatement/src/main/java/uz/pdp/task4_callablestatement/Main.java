package uz.pdp.task4_callablestatement;

public class Main {
    public static void main(String[] args) {
        Product product = new Product("maker2", "model2","type2");
        ProductService productService = new ProductService();
        System.out.println(productService.addProduct(product));
    }
}
