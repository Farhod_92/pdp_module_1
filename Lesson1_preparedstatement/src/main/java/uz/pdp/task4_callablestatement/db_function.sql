CREATE OR REPLACE FUNCTION public.add_product(IN in_maker character varying,IN in_model character varying,IN in_type character varying)
    RETURNS boolean
    LANGUAGE 'plpgsql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100

AS $BODY$
declare
	i int:=0;
begin
	select count(*) into i from product where model=in_model and maker=in_maker;
	if i=0 then
		insert into product values(in_maker, in_model, in_type);
		return true;
	else
		return false;
	end if;

end
$BODY$;