package uz.pdp.lesson;

import java.sql.*;

public class DatabaseService {
    private String url = "jdbc:postgresql://localhost:5432/prepared_statement";
    private String username = "postgres";
    private String password = "123";

    public void saveUserPrepared(User user){
        try {
            Connection connection = DriverManager.getConnection(url,username,password);
            String query = "insert into users(username) values(?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.execute();
            System.out.println("user add");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void saveUser(User user){
        try {
            Connection connection = DriverManager.getConnection(url,username,password);
            Statement statement = connection.createStatement();
            String query = "insert into users(username) values('"+user.getUsername()+"')";
            statement.execute(query);
            System.out.println("user add");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void getUsersPrepared(){
        try {
            Connection connection = DriverManager.getConnection(url,username,password);
            String query = "select * from users";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println("user list");
            while (resultSet.next()){
                System.out.println(resultSet.getString(2));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void getUsers(){
        try {
            Connection connection = DriverManager.getConnection(url,username,password);
            Statement statement = connection.createStatement();
            String query = "select * from users";
            ResultSet resultSet = statement.executeQuery(query);
            System.out.println("user list");
            while (resultSet.next()){
                System.out.println(resultSet.getString(2));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void deleteUser(int id){
        try {
            Connection connection = DriverManager.getConnection(url,username,password);
            Statement statement = connection.createStatement();
            String query = "delete  from users where  id="+id;
            int i = statement.executeUpdate(query);
            if(i==0)
                System.out.println("user not  deleted");
            else
                System.out.println("user deleted");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void editUser(User user){
        try {
            Connection connection = DriverManager.getConnection(url,username,password);
            Statement statement = connection.createStatement();
            String query = "update users set username='"+user.getUsername() + "'where id=" + user.getId();
            int i = statement.executeUpdate(query);
            if(i==0)
                System.out.println("user not  edited");
            else
                System.out.println("user edited");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }



}
