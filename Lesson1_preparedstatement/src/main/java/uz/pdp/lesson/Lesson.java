package uz.pdp.lesson;

import java.util.Scanner;

public class Lesson {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DatabaseService databaseService = new DatabaseService();

        int i = -1;

        while ((i != 0)) {
            System.out.println("0=>Exit, 1=>New user, 2=>Edit user, 3=> Delete user,4=>List users");
            i = scanner.nextInt();
            scanner = new Scanner(System.in);

            switch (i){
                case 1:
                    System.out.println("username:");
                    String username = scanner.nextLine();
                    User user = new User(username);
                    databaseService.saveUserPrepared(user);
                    break;
                case 2:
                    System.out.println("editing user id:");
                    int id = scanner.nextInt();
                    System.out.println("editing user username:");
                    scanner = new Scanner(System.in);
                    username = scanner.nextLine();
                    user = new User(id, username);
                    databaseService.editUser(user);
                    break;
                case 3:
                    System.out.println("oxhiriladigan user id:");
                    id = scanner.nextInt();
                    databaseService.deleteUser(id);
                    break;
                case 4:
                    databaseService.getUsersPrepared();
                    break;
            }
        }
    }

}
