package uz.pdp.lesson9_1.repostitory;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson9_1.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {
}
