package uz.pdp.lesson9_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.lesson9_1.entity.Attachment;
import uz.pdp.lesson9_1.repostitory.AttachmentRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping("/attachment")
public class AttachmentController {
    @Autowired
    private AttachmentRepository attachmentRepository;

    private final String filesPaths = "downloads";

    @PostMapping("/upload")
    public String uploadFile(MultipartHttpServletRequest request) throws IOException {
        //ATTACHMENT
        List<Integer> savedAttachmentIds = new ArrayList<>();

        try{
            Iterator<String> fileNames = request.getFileNames();
            while (fileNames.hasNext()) {
                MultipartFile file = request.getFile(fileNames.next());
                if (file != null) {
                    String originalFilename = file.getOriginalFilename();
                    long size = file.getSize();
                    String contentType = file.getContentType();

                    Attachment attachment = new Attachment();
                    attachment.setFileOriginalName(originalFilename);
                    attachment.setSize(size);
                    attachment.setContentType(contentType);
                    String[] split = originalFilename.split("\\.");
                    String name = UUID.randomUUID().toString() + "." + split[split.length - 1];
                    attachment.setName(name);
                    Attachment savedAttachment = attachmentRepository.save(attachment);

                    Path path = Paths.get(filesPaths + "/" + name);
                    Files.copy(file.getInputStream(), path);
                    savedAttachmentIds.add(attachment.getId());
                }
            }
            return "fayllar saqlandi. ID=" + savedAttachmentIds;
        }catch (Exception e){
            return "Xatolik:" + e.getMessage();
        }
    }

    @GetMapping
    public List<Attachment> info(){
        return attachmentRepository.findAll();
    }

    @GetMapping("/download/{attachmentId}")
    public void download(@PathVariable Integer attachmentId, HttpServletResponse response) throws IOException {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(attachmentId);
        if(optionalAttachment.isPresent()){
            Attachment attachment = optionalAttachment.get();
            FileInputStream inputStream = new FileInputStream(filesPaths + "/" + attachment.getName());
            response.setHeader("Content-Disposition", "attachment; filename=\""+attachment.getFileOriginalName()+"\""); //DOWNLOAD
            response.setContentType(attachment.getContentType());
            FileCopyUtils.copy(inputStream, response.getOutputStream());
        }
    }
}
