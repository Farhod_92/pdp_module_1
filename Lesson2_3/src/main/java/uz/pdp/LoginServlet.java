package uz.pdp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class LoginServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        resp.setContentType("text/html");
        writer.write("<h1> Users page </h1>");
        writer.write("<a href='/cabinet'> Cabinet </a>");
        writer.write("<form action=\"/login\" method=\"post\">\n" +
                "    <input type=\"text\" name=\"login\" placeholder=\"login\">\n" +
                "    <input type=\"password\" name=\"parol\" placeholder=\"parol\">\n" +
                "    <button type=\"submit\">Saqlash</button>\n" +
                "</form>");
    }

    public  static List<User> userList = new ArrayList<User>();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String parol = req.getParameter("parol");
        userList.add(new User(login, parol));

        PrintWriter writer = resp.getWriter();
        writer.write("<h1> Users page </h1>");
        writer.write("<a href='/cabinet'> Cabinet </a>");
        writer.write("<form action=\"/login\" method=\"post\">\n" +
                "    <input type=\"text\" name=\"login\" placeholder=\"login\">\n" +
                "    <input type=\"password\" name=\"parol\" placeholder=\"parol\">\n" +
                "    <button type=\"submit\">Saqlash</button>\n" +
                "</form>");

        for (User user : userList) {
            writer.write(user.getLogin() +" : "+ user.getParol() + "<br>");
        }
    }
}
