package uz.pdp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CabinetServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("cabinet.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String parol = req.getParameter("parol");
        PrintWriter writer = resp.getWriter();
        boolean conatins=false;
        for (User user : LoginServlet.userList) {
            if(user.getLogin().equals(login) && user.getParol().equals(parol)) {
                conatins=true;
                break;
            }
        }
        if(conatins)
                writer.write("Tizimga xush kelibsiz");
        else
                writer.write("Bunday foydalanuvchi mavjud emas");
    }
}
