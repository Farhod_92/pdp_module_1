package uz.pdp;

public class User {
    private String login;
    private String parol;


    public User() {
    }

    public User(String login, String parol) {
        this.login = login;
        this.parol = parol;
    }

    public String getLogin() {
        return login;
    }

    public String getParol() {
        return parol;
    }
}
