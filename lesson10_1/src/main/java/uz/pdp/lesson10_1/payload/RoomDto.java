package uz.pdp.lesson10_1.payload;

import lombok.Getter;

@Getter
public class RoomDto {
    private Integer hotelId;

    private Integer number;

    private Integer floor;

    private Integer size;
}
