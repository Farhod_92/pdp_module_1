package uz.pdp.lesson10_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson10_1.entity.Hotel;
import uz.pdp.lesson10_1.entity.Room;
import uz.pdp.lesson10_1.payload.RoomDto;
import uz.pdp.lesson10_1.repository.HotelRepository;
import uz.pdp.lesson10_1.repository.RoomRepository;

import java.util.Optional;

@RestController
@RequestMapping("/room")
public class RoomController {
    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private RoomRepository roomRepository;

    @PostMapping
    public String add(@RequestBody RoomDto roomDto){
        Optional<Hotel> optionalHotel = hotelRepository.findById(roomDto.getHotelId());
        if(!optionalHotel.isPresent())
            return "hotel not found";
        Room room = new Room();
        room.setFloor(roomDto.getFloor());
        room.setNumber(roomDto.getNumber());
        room.setSize(roomDto.getSize());
        room.setHotel(optionalHotel.get());
        Room save = roomRepository.save(room);
        return "room saved. id:" + save.getId();
    }

    @GetMapping("/{hotelId}")
    public Page<Room> getHotelRoomsPageable(@PathVariable Integer hotelId, @RequestParam int page){
        Pageable pageable = PageRequest.of(page, 2);
        return roomRepository.findAllByHotel_Id(hotelId, pageable);
    }
}
