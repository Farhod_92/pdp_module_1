package uz.pdp.lesson10_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.lesson10_1.entity.Hotel;
import uz.pdp.lesson10_1.repository.HotelRepository;

@RestController
@RequestMapping("/hotel")
public class HotelController {
    @Autowired
    private HotelRepository hotelRepository;

    @PostMapping
    public String add(@RequestBody Hotel hotel){
        hotelRepository.save(hotel);
        return "hotel saved:" + hotel.getName();
    }
}
