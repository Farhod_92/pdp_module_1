package uz.pdp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DbConnect {
    private static String url = "jdbc:postgresql://localhost:5432/computer_db";
    private static String username = "postgres";
    private static String password = "123";

    public Connection getConnection(){
        try {
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }
}
