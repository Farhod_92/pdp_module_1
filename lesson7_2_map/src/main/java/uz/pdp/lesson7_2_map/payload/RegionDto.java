package uz.pdp.lesson7_2_map.payload;

import lombok.Getter;

@Getter
public class RegionDto {
    private String name;
    private int countryId;
}
