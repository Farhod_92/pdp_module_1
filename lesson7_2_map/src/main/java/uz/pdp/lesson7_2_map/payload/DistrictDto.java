package uz.pdp.lesson7_2_map.payload;

import lombok.Getter;

@Getter
public class DistrictDto {
    private String name;
    private int regionId;
}
