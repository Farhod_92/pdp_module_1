package uz.pdp.lesson7_2_map.payload;

import lombok.Getter;

@Getter
public class StudentDto {
    private String name;
    private int groupId;
}
