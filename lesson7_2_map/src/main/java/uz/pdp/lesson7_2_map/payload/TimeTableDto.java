package uz.pdp.lesson7_2_map.payload;

import lombok.Getter;

@Getter
public class TimeTableDto {
    private int groupId;
    private int subjectId;
    private int teacherId;
}
