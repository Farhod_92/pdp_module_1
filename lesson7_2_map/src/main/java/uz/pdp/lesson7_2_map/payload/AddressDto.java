package uz.pdp.lesson7_2_map.payload;

import lombok.Getter;

@Getter
public class AddressDto {
    private String street;
    private String homeNumber;
    private int districtId;
}
