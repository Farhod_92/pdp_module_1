package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.University;
import uz.pdp.lesson7_2_map.repository.UniverRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/university")
public class UniversityController {
    @Autowired
    private UniverRepository univerRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody University university){
        University savedUniver = univerRepository.save(university);
        return "univer saved:" + savedUniver.getName();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public University getById(@PathVariable Integer id){
        Optional<University> optionalUniversity = univerRepository.findById(id);
        if(!optionalUniversity.isPresent())
            return null;
        else
            return optionalUniversity.get();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<University> getAllContinents(){
        return univerRepository.findAll();
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody University university){
        Optional<University> optionalUniversity = univerRepository.findById(id);
        if(!optionalUniversity.isPresent())
            return "univer topilmadi";

        University editingUniver = optionalUniversity.get();
        editingUniver.setName(university.getName());
        univerRepository.save(editingUniver);
        return "university o'zgartirildi:" +  editingUniver.getName();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{continentId}")
    public String deleteContinent(@PathVariable Integer continentId){
        try {
            univerRepository.deleteById(continentId);
            return "univer o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }

}
