package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.District;
import uz.pdp.lesson7_2_map.entity.Faculty;
import uz.pdp.lesson7_2_map.entity.Region;
import uz.pdp.lesson7_2_map.entity.University;
import uz.pdp.lesson7_2_map.payload.DistrictDto;
import uz.pdp.lesson7_2_map.payload.FacultyDto;
import uz.pdp.lesson7_2_map.repository.DistrictRepository;
import uz.pdp.lesson7_2_map.repository.FacultyRepository;
import uz.pdp.lesson7_2_map.repository.RegionRepository;
import uz.pdp.lesson7_2_map.repository.UniverRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/faculty")
public class FacultyController {
    @Autowired
    private FacultyRepository facultyRepository;

    @Autowired
    private UniverRepository univerRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody FacultyDto facultyDto){

        boolean existsByNameAndUniversityId = facultyRepository.existsByNameAndUniversityId(facultyDto.getName(), facultyDto.getUniverId());
        if(existsByNameAndUniversityId)
            return "bu univerda bunaqa facultet bor";

        Optional<University> optionalUniversity = univerRepository.findById(facultyDto.getUniverId());
        if(!optionalUniversity.isPresent())
            return "univer topilmadi";


        Faculty faculty = new Faculty();
        faculty.setName(facultyDto.getName());
        faculty.setUniversity(optionalUniversity.get());
        Faculty savedFaculty = facultyRepository.save(faculty);
        return "facultet saqlandi:" + savedFaculty.toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public Faculty getById(@PathVariable Integer id){
        Optional<Faculty> optionalFaculty= facultyRepository.findById(id);
        return optionalFaculty.orElse(null);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Faculty> getAll(){
        return facultyRepository.findAll();
    }

    @GetMapping("/byUniverId/{id}")
    public List<Faculty> getByUniverId(@PathVariable Integer id){
        return facultyRepository.findAllByUniversityId(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody FacultyDto facultyDto){
        Optional<Faculty> optionalFaculty = facultyRepository.findById(id);
        if(!optionalFaculty.isPresent())
            return "facultet topilmadi";

        Optional<University> optionalUniversity = univerRepository.findById(id);
        if(!optionalUniversity.isPresent())
            return "univer topilmadi";

        Faculty editingFaculty = optionalFaculty.get();
        editingFaculty.setName(facultyDto.getName());
        editingFaculty.setUniversity(optionalUniversity.get());
        facultyRepository.save(editingFaculty);
        return "facultet o'zgartirildi:" +  editingFaculty.toString();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String delete(@PathVariable Integer id){
        try {
            facultyRepository.deleteById(id);
            return "facultet o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }
}
