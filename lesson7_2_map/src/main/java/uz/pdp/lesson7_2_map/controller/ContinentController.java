package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.Continent;
import uz.pdp.lesson7_2_map.repository.ContinentRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/continent")
public class ContinentController {

    @Autowired
    private ContinentRepository continentRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String saveContinent(@RequestBody Continent continent){
        Continent savedContinent = continentRepository.save(continent);
        return "continent saved:" + savedContinent.getName();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{continentId}")
    public Continent getContinentById(@PathVariable Integer continentId){
        Optional<Continent> optionalContinent = continentRepository.findById(continentId);
        if(!optionalContinent.isPresent())
            return null;
        else
            return optionalContinent.get();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Continent> getAllContinents(){
        return continentRepository.findAll();
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{continentId}")
    public String editContinent(@PathVariable Integer continentId, @RequestBody Continent continent){
        Optional<Continent> optionalContinent = continentRepository.findById(continentId);
        if(!optionalContinent.isPresent())
            return "continent topilmadi";

        Continent editingContinent = optionalContinent.get();
        editingContinent.setName(continent.getName());
        continentRepository.save(editingContinent);
        return "continent o'zgartirildi:" +  editingContinent.getName();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{continentId}")
    public String deleteContinent(@PathVariable Integer continentId){
        try {
            continentRepository.deleteById(continentId);
            return "continent o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }

}
