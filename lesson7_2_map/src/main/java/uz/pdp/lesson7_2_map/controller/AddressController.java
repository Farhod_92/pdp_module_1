package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.Address;
import uz.pdp.lesson7_2_map.entity.District;
import uz.pdp.lesson7_2_map.entity.Region;
import uz.pdp.lesson7_2_map.payload.AddressDto;
import uz.pdp.lesson7_2_map.payload.DistrictDto;
import uz.pdp.lesson7_2_map.repository.AddressRepository;
import uz.pdp.lesson7_2_map.repository.DistrictRepository;
import uz.pdp.lesson7_2_map.repository.RegionRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody AddressDto addressDto){
        Optional<District> optionalDistrict = districtRepository.findById(addressDto.getDistrictId());
        if(!optionalDistrict.isPresent())
            return "district topilmadi";

        Address address = new Address();
        address.setStreet(addressDto.getStreet());
        address.setHomeNumber(addressDto.getHomeNumber());
        address.setDistrict(optionalDistrict.get());
        Address savedAddress = addressRepository.save(address);
        return "address saqlandi:" + savedAddress.toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public Address getById(@PathVariable Integer id){
        Optional<Address> optionalAddress= addressRepository.findById(id);
        return optionalAddress.orElse(null);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Address> getAll(){
        return addressRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody AddressDto addressDto){
        Optional<Address> optionalAddress = addressRepository.findById(id);
        if(!optionalAddress.isPresent())
            return "address topilmadi";

        Optional<District> optionalDistrict = districtRepository.findById(addressDto.getDistrictId());
        if(!optionalDistrict.isPresent())
            return "district topilmadi";

        Address editingAddress =optionalAddress.get();
        editingAddress.setStreet(addressDto.getStreet());
        editingAddress.setHomeNumber(addressDto.getHomeNumber());
        editingAddress.setDistrict(optionalDistrict.get());
        addressRepository.save(editingAddress);
        return "address o'zgartirildi:" +  editingAddress.toString();
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String deleteContinent(@PathVariable Integer id){
        try {
            addressRepository.deleteById(id);
            return "address o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }

}
