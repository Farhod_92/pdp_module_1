package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.Continent;
import uz.pdp.lesson7_2_map.entity.Country;
import uz.pdp.lesson7_2_map.entity.Region;
import uz.pdp.lesson7_2_map.payload.CountryDto;
import uz.pdp.lesson7_2_map.payload.RegionDto;
import uz.pdp.lesson7_2_map.repository.ContinentRepository;
import uz.pdp.lesson7_2_map.repository.CountryRepository;
import uz.pdp.lesson7_2_map.repository.RegionRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/region")
public class RegionController {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private RegionRepository regionRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody RegionDto regionDto){
        Optional<Country> optionalCountry = countryRepository.findById(regionDto.getCountryId());
        if(!optionalCountry.isPresent())
            return "country topilmadi";

        Region region = new Region();
        region.setName(regionDto.getName());
        region.setCountry(optionalCountry.get());
        Region savedRegion = regionRepository.save(region);
        return "region saqlandi:" + savedRegion.toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public Region getById(@PathVariable Integer id){
        Optional<Region> optionalRegion = regionRepository.findById(id);
        if(!optionalRegion.isPresent())
            return null;
        else
            return optionalRegion.get();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Region> getAll(){
        return regionRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody RegionDto regionDto){
        Optional<Region> optionalRegion = regionRepository.findById(id);
        if(!optionalRegion.isPresent())
            return "region topilmadi";

        Optional<Country> optionalCountry = countryRepository.findById(regionDto.getCountryId());
        if(!optionalCountry.isPresent())
            return "country topilmadi";

        Region editingRegion =optionalRegion.get();
        editingRegion.setName(regionDto.getName());
        editingRegion.setCountry(optionalCountry.get());
        regionRepository.save(editingRegion);
        return "region o'zgartirildi:" +  editingRegion.toString();
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String deleteContinent(@PathVariable Integer id){
        try {
            regionRepository.deleteById(id);
            return "region o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }

}
