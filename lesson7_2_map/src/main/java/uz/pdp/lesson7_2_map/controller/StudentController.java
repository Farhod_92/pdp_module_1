package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.Faculty;
import uz.pdp.lesson7_2_map.entity.Groups;
import uz.pdp.lesson7_2_map.entity.Student;
import uz.pdp.lesson7_2_map.payload.GroupsDto;
import uz.pdp.lesson7_2_map.payload.StudentDto;
import uz.pdp.lesson7_2_map.repository.FacultyRepository;
import uz.pdp.lesson7_2_map.repository.GroupRepository;
import uz.pdp.lesson7_2_map.repository.StudentRepository;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private StudentRepository studentRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody StudentDto studentDto){
        Optional<Groups> optionalGroups = groupRepository.findById(studentDto.getGroupId());
        if(!optionalGroups.isPresent())
            return "group topilmadi";

        Student student = new Student();
        student.setName(studentDto.getName());
        student.setGroups(optionalGroups.get());
        Student savedStudent = studentRepository.save(student);
        return "student saqlandi:" + savedStudent.toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public Student getById(@PathVariable Integer id){
        Optional<Student> optionalStudent = studentRepository.findById(id);
        return optionalStudent.orElse(null);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Student> getAll(){
        return studentRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody StudentDto studentDto){

        Optional<Student> optionalStudent = studentRepository.findById(id);
        if(!optionalStudent.isPresent())
            return "student topilmadi";

        Optional<Groups> optionalGroups = groupRepository.findById(studentDto.getGroupId());
        if(!optionalGroups.isPresent())
            return "group topilmadi";

        Student student = optionalStudent.get();
        student.setName(studentDto.getName());
        student.setGroups(optionalGroups.get());
        Student savedStudent = studentRepository.save(student);
        return "student edited:" + savedStudent.toString();
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String delete(@PathVariable Integer id){
        try {
            studentRepository.deleteById(id);
            return "group o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }
}
