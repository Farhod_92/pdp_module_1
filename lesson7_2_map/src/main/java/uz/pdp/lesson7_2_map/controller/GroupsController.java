package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.District;
import uz.pdp.lesson7_2_map.entity.Faculty;
import uz.pdp.lesson7_2_map.entity.Groups;
import uz.pdp.lesson7_2_map.entity.Region;
import uz.pdp.lesson7_2_map.payload.DistrictDto;
import uz.pdp.lesson7_2_map.payload.FacultyDto;
import uz.pdp.lesson7_2_map.payload.GroupsDto;
import uz.pdp.lesson7_2_map.repository.DistrictRepository;
import uz.pdp.lesson7_2_map.repository.FacultyRepository;
import uz.pdp.lesson7_2_map.repository.GroupRepository;
import uz.pdp.lesson7_2_map.repository.RegionRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/groups")
public class GroupsController {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private FacultyRepository facultyRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody GroupsDto groupsDto){
        Optional<Faculty> optionalFaculty = facultyRepository.findById(groupsDto.getFacultyId());
        if(!optionalFaculty.isPresent())
            return "faculty topilmadi";

        Groups group = new Groups();
        group.setName(groupsDto.getName());
        group.setFaculty(optionalFaculty.get());
        Groups savedGroup = groupRepository.save(group);
        return "group saqlandi:" + savedGroup.toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public Groups getById(@PathVariable Integer id){
        Optional<Groups> optionalGroups = groupRepository.findById(id);
        return optionalGroups.orElse(null);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Groups> getAll(){
        return groupRepository.findAll();
    }

    @GetMapping("/getByUniverId/{id}")
    public List<Groups> getByUniverId(@PathVariable Integer id){
        List<Groups> allByFaculty_university_id = groupRepository.findAllByFaculty_University_Id(id);
        List<Groups> groupsByUniversityId = groupRepository.getGroupsByUniversityId(id);
        List<Groups> groupsByUniversityIdNative = groupRepository.getGroupsByUniversityIdNative(id);
        return allByFaculty_university_id;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody GroupsDto groupsDto){
        Optional<Faculty> optionalFaculty = facultyRepository.findById(groupsDto.getFacultyId());
        if(!optionalFaculty.isPresent())
            return "faculty topilmadi";

        Optional<Groups> optionalGroups = groupRepository.findById(id);
        if(!optionalGroups.isPresent())
            return "group topilmadi";

        Groups group = optionalGroups.get();
        group.setName(groupsDto.getName());
        group.setFaculty(optionalFaculty.get());
        Groups savedGroup = groupRepository.save(group);
        return "group edited:" + savedGroup.toString();
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String deleteContinent(@PathVariable Integer id){
        try {
            groupRepository.deleteById(id);
            return "group o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }
}
