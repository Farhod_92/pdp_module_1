package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.Continent;
import uz.pdp.lesson7_2_map.entity.Teacher;
import uz.pdp.lesson7_2_map.repository.ContinentRepository;
import uz.pdp.lesson7_2_map.repository.TeacherRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    private TeacherRepository teacherRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody Teacher teacher){
        Teacher savedTeacher = teacherRepository.save(teacher);
        return "teacher saved:" + savedTeacher.getName();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public Teacher getById(@PathVariable Integer id){
        Optional<Teacher> optionalTeacher = teacherRepository.findById(id);
        if(!optionalTeacher.isPresent())
            return null;
        else
            return optionalTeacher.get();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Teacher> getAllContinents(){
        return teacherRepository.findAll();
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody Teacher teacher){
        Optional<Teacher> optionalTeacher = teacherRepository.findById(id);
        if(!optionalTeacher.isPresent())
            return "continent topilmadi";

        Teacher editingTeacher = optionalTeacher.get();
        editingTeacher.setName(teacher.getName());
        teacherRepository.save(teacher);
        return "tacher o'zgartirildi:" +  editingTeacher.getName();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String delete(@PathVariable Integer id){
        try {
            teacherRepository.deleteById(id);
            return "teacher o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }

}
