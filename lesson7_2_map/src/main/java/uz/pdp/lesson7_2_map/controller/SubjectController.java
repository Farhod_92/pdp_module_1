package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.lesson7_2_map.entity.Subject;
import uz.pdp.lesson7_2_map.repository.SubjectRepository;

import java.util.List;

@RestController
@RequestMapping(value = "/subject")
public class SubjectController {

    @Autowired
    private SubjectRepository subjectRepository;

    @RequestMapping(method = RequestMethod.POST)
    public String add(@RequestBody Subject subject){
        if(subjectRepository.existsByName(subject.getName()))
            return "this subject already exists";
        subjectRepository.save(subject);
        return "subject saved";
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Subject> getAll(){
        return subjectRepository.findAll();
    }
}
