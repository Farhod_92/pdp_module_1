package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.Groups;
import uz.pdp.lesson7_2_map.entity.Subject;
import uz.pdp.lesson7_2_map.entity.Teacher;
import uz.pdp.lesson7_2_map.entity.TimeTable;
import uz.pdp.lesson7_2_map.payload.TimeTableDto;
import uz.pdp.lesson7_2_map.repository.GroupRepository;
import uz.pdp.lesson7_2_map.repository.SubjectRepository;
import uz.pdp.lesson7_2_map.repository.TeacherRepository;
import uz.pdp.lesson7_2_map.repository.TimeTableRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/timeTable")
public class TimeTableController {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private TimeTableRepository timeTableRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody TimeTableDto timeTableDto){
        Optional<Groups> optionalGroups = groupRepository.findById(timeTableDto.getGroupId());
        if(!optionalGroups.isPresent())
            return "group topilmadi";

        Optional<Teacher>optionalTeacher = teacherRepository.findById(timeTableDto.getTeacherId());
        if(!optionalTeacher.isPresent())
            return "teacher topilmadi";

        Optional<Subject> optionalSubject = subjectRepository.findById(timeTableDto.getSubjectId());
        if(!optionalSubject.isPresent())
            return "subject topilmadi";

        TimeTable timeTable = new TimeTable();
        timeTable.setGroups(optionalGroups.get());
        timeTable.setSubject(optionalSubject.get());
        timeTable.setTeacher(optionalTeacher.get());
        TimeTable savedTimeTable = timeTableRepository.save(timeTable);
        return "timetable saqlandi:" + timeTable.toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public TimeTable getById(@PathVariable Integer id){
        Optional<TimeTable> optionalTimeTable = timeTableRepository.findById(id);
        if(!optionalTimeTable.isPresent())
            return null;
        else
            return optionalTimeTable.get();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<TimeTable> getAll(){
        return timeTableRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody TimeTableDto timeTableDto){

        Optional<TimeTable> optionalTimeTable = timeTableRepository.findById(id);
        if(!optionalTimeTable.isPresent())
            return "timetable topilmadi";

        Optional<Groups> optionalGroups = groupRepository.findById(timeTableDto.getGroupId());
        if(!optionalGroups.isPresent())
            return "group topilmadi";

        Optional<Teacher>optionalTeacher = teacherRepository.findById(timeTableDto.getTeacherId());
        if(!optionalTeacher.isPresent())
            return "teacher topilmadi";

        Optional<Subject> optionalSubject = subjectRepository.findById(timeTableDto.getSubjectId());
        if(!optionalSubject.isPresent())
            return "subject topilmadi";

        TimeTable timeTable = optionalTimeTable.get();
        timeTable.setGroups(optionalGroups.get());
        timeTable.setSubject(optionalSubject.get());
        timeTable.setTeacher(optionalTeacher.get());
        TimeTable savedTimeTable = timeTableRepository.save(timeTable);
        return "timetable o'zgartirildi:" +  savedTimeTable.toString();
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String deleteContinent(@PathVariable Integer id){
        try {
            timeTableRepository.deleteById(id);
            return "timetable o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }

}
