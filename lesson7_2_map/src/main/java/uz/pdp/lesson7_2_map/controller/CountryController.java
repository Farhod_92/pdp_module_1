package uz.pdp.lesson7_2_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson7_2_map.entity.Continent;
import uz.pdp.lesson7_2_map.entity.Country;
import uz.pdp.lesson7_2_map.payload.CountryDto;
import uz.pdp.lesson7_2_map.repository.ContinentRepository;
import uz.pdp.lesson7_2_map.repository.CountryRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/country")
public class CountryController {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private ContinentRepository continentRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody CountryDto countryDto){
        Optional<Continent> optionalContinent = continentRepository.findById(countryDto.getContinentId());
        if(!optionalContinent.isPresent())
            return "continent topilmadi";

        Country country = new Country();
        country.setName(countryDto.getName());
        country.setContinent(optionalContinent.get());
        Country savedCountry = countryRepository.save(country);
        return "continent saqlandi:" + savedCountry.toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public Country getById(@PathVariable Integer id){
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if(!optionalCountry.isPresent())
            return null;
        else
            return optionalCountry.get();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Country> getAll(){
        return countryRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody CountryDto countryDto){
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if(!optionalCountry.isPresent())
            return "country topilmadi";

        Optional<Continent> optionalContinent = continentRepository.findById(countryDto.getContinentId());
        if(!optionalContinent.isPresent())
            return "continent topilmadi";

        Country editingCountry = optionalCountry.get();
        editingCountry.setName(countryDto.getName());
        editingCountry.setContinent(optionalContinent.get());
        countryRepository.save(editingCountry);
        return "country o'zgartirildi:" +  editingCountry.toString();
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String deleteContinent(@PathVariable Integer id){
        try {
            countryRepository.deleteById(id);
            return "country o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }



}
