package uz.pdp.lesson7_2_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson7_2_map.entity.Country;
import uz.pdp.lesson7_2_map.entity.Region;

public interface RegionRepository extends JpaRepository<Region, Integer> {
}
