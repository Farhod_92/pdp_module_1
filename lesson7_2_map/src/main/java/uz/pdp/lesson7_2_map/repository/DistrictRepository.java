package uz.pdp.lesson7_2_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson7_2_map.entity.District;
import uz.pdp.lesson7_2_map.entity.Region;

public interface DistrictRepository extends JpaRepository<District, Integer> {
}
