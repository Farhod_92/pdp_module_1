package uz.pdp.lesson7_2_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.lesson7_2_map.entity.District;
import uz.pdp.lesson7_2_map.entity.Groups;

import java.util.List;

public interface GroupRepository extends JpaRepository<Groups, Integer> {
    List<Groups> findAllByFaculty_University_Id(Integer faculty_university_id);

   // @Query("select gr from groups gr where gr.faculty.university.id=?1")
    @Query("select gr from Groups gr where gr.faculty.university.id=:universityId")
    List<Groups> getGroupsByUniversityId(Integer universityId);

    @Query(value = "select *\n" +
            "from groups gr\n" +
            "         join faculty f on f.id=gr.faculty_id\n" +
            "         join university u on u.id = f.university_id\n" +
            "where u.id=:universityId", nativeQuery = true)
    List<Groups> getGroupsByUniversityIdNative(Integer universityId);
}
