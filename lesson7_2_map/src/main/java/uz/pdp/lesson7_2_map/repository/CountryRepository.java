package uz.pdp.lesson7_2_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson7_2_map.entity.Continent;
import uz.pdp.lesson7_2_map.entity.Country;

public interface CountryRepository extends JpaRepository<Country, Integer> {
}
