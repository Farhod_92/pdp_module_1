package uz.pdp.lesson7_2_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson7_2_map.entity.Address;
import uz.pdp.lesson7_2_map.entity.University;

public interface UniverRepository extends JpaRepository<University, Integer> {
}
