package uz.pdp.lesson7_2_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson7_2_map.entity.Faculty;
import uz.pdp.lesson7_2_map.entity.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
}
