package uz.pdp.lesson7_2_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson7_2_map.entity.Groups;
import uz.pdp.lesson7_2_map.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
