package uz.pdp.lesson7_2_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson7_2_map.entity.Address;
import uz.pdp.lesson7_2_map.entity.TimeTable;

public interface TimeTableRepository extends JpaRepository<TimeTable, Integer> {
}
