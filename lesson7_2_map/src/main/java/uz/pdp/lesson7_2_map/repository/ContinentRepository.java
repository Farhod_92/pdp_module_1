package uz.pdp.lesson7_2_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson7_2_map.entity.Continent;

public interface ContinentRepository extends JpaRepository<Continent, Integer> {
}
