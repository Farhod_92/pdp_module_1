package uz.pdp.lesson7_2_map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson72MapApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson72MapApplication.class, args);
    }

}
