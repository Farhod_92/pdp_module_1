package uz.pdp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

public class Laptop extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            DbConnect dbConnect =new DbConnect();
            Connection connection = dbConnect.getConnection();
            String pcs = "select maker, p.model, price from product p join laptop l  on p.model = l.model order by price asc";

            PreparedStatement preparedStatement = connection.prepareStatement(pcs);
            ResultSet resultSet = preparedStatement.executeQuery();

            PrintWriter printWriter = resp.getWriter();
            printWriter.write("<h1> LAPTOPS </h1>");
            printWriter.write("<table>\n" +
                    "  <tr>\n" +
                    "    <th>Maker</th>\n" +
                    "    <th>Model</th>\n" +
                    "    <th>Price</th>\n" +
                    "  </tr>");
            while (resultSet.next()){
                printWriter.write("<tr>");
                printWriter.write("<td>" + resultSet.getString(1) + "</td>");
                printWriter.write("<td>" + resultSet.getString(2) + "</td>");
                printWriter.write("<td>" + resultSet.getFloat(3) + "</td>");
                printWriter.write("</tr>");
                System.out.println(resultSet.getString(1) );
            }

            printWriter.write("</table>");
            preparedStatement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
