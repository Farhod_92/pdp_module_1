<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<table>
  <tr>
      <th>Course number</th>
      <th>Course name</th>
      <th>Course info</th>
  </tr>
    <c:forEach items="${courseList}" var="course">
        <tr>
            <td> <c:out value="${course.courseNumber}"/></td>
            <td> <c:out value="${course.courseName}"/></td>
            <td> <c:out value="${course.courseInfo}"/></td>
        </tr>
    </c:forEach>
</table>

<form action="/course" method="post">
    <input type="text" name="number" placeholder="Course number">
    <input type="text" name="name" placeholder="Course name">
    <input type="text" name="info" placeholder="Course info">
    <button type="submit"> Add </button>
</form>


</body>
</html>
