package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
public class Course {
    private Integer courseNumber;
    private String courseName;
    private String courseInfo;

    public Course() {
        this.courseNumber = 12;
        this.courseName = "Spring";
        this.courseInfo = "Info";
    }
}
