package uz.pdp.model;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class Student {
    private Integer id;
    private String firstName;
    private Integer birthYear;

    public Student(){
        this.id = 1;
        this.firstName = "Abbos Tohirov";
        this.birthYear = 1995;
    }

}
