package uz.pdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.pdp.model.Course;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class CourseController {
    List<Course> courseList = new ArrayList<Course>(Arrays.asList(
            new Course(1, "Java", "Info java"),
            new Course(2, "Spring", "Info spring"),
            new Course(3, "C#", "Info c#"),
            new Course(4, "Kotlin", "Info Kotlin"),
            new Course(5, "MongoDb", "Info MongoDb")
    ));

    @RequestMapping(value = "/course", method = RequestMethod.GET)
    public String getCourses(Model model){
        model.addAttribute("courseList",courseList);
        return "course";
    }

    @RequestMapping(value = "/course", method = RequestMethod.POST)
    public String addCourse(HttpServletRequest request, Model model){
        Integer number = Integer.valueOf(request.getParameter("number"));
        String name = request.getParameter("name");
        String info = request.getParameter("info");
        courseList.add(new Course(number, name, info));
        return "redirect:/course";
    }
}
