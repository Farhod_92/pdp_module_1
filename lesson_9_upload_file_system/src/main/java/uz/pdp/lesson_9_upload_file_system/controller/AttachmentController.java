package uz.pdp.lesson_9_upload_file_system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.lesson_9_upload_file_system.entity.Attachment;
import uz.pdp.lesson_9_upload_file_system.repostitory.AttachmentRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/attachment")
public class AttachmentController {
    @Autowired
    private AttachmentRepository attachmentRepository;

    private static  final String uploadDirectory = "downloads";

    @PostMapping("/upload")
    public String uploadToFileSystem(MultipartHttpServletRequest request) throws IOException {
        long l = System.currentTimeMillis();
        //ATTACHMENT
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());
        if(file != null) {
            String originalFilename = file.getOriginalFilename();
            long size = file.getSize();
            String contentType = file.getContentType();

            Attachment attachment = new Attachment();
            attachment.setFileOriginalName(originalFilename);
            attachment.setSize(size);
            attachment.setContentType(contentType);
            String[] split = originalFilename.split("\\.");
            String name = UUID.randomUUID().toString() + "." + split[split.length-1];
            attachment.setName(name);
            Attachment savedAttachment = attachmentRepository.save(attachment);

            Path path = Paths.get(uploadDirectory + "/" + name);
            Files.copy(file.getInputStream(), path);
            System.out.println(System.currentTimeMillis()-l);
            return "fayl sistemaga saqlandi. ID=" + savedAttachment.getId();
        }
        return "Xatolik";
    }

    @GetMapping("/download/{attachmentId}")
    public void download(@PathVariable Integer attachmentId, HttpServletResponse response) throws IOException {
        long l = System.currentTimeMillis();
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(attachmentId);
        if(optionalAttachment.isPresent()){
            Attachment attachment = optionalAttachment.get();

  //          response.setHeader("Content-Disposition", "inline"); // FAQAT KO'RSATISH
//                //response.setHeader("Content-Disposition", "attachment"); // DOWNLOAD
               response.setHeader("Content-Disposition", "attachment; filename=\""+attachment.getFileOriginalName()+"\""); //DOWNLOAD
            response.setContentType(attachment.getContentType());

            FileInputStream inputStream = new FileInputStream(uploadDirectory + "/" + attachment.getName());
            FileCopyUtils.copy(inputStream, response.getOutputStream());
        }
        System.out.println(System.currentTimeMillis()-l);
    }
}
