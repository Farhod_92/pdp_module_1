package uz.pdp.lesson_9_upload_file_system.repostitory;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson_9_upload_file_system.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {
}
