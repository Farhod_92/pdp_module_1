package uz.pdp.lesson_9_upload_file_system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson9UploadFileSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson9UploadFileSystemApplication.class, args);
    }

}
