package uz.pdp.lesson8_1_uzb_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson8_1_uzb_map.entity.Car;
import uz.pdp.lesson8_1_uzb_map.entity.Region;
import uz.pdp.lesson8_1_uzb_map.entity.Users;
import uz.pdp.lesson8_1_uzb_map.payload.CarDto;
import uz.pdp.lesson8_1_uzb_map.repository.CarRepository;
import uz.pdp.lesson8_1_uzb_map.repository.RegionRepository;
import uz.pdp.lesson8_1_uzb_map.repository.UsersRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/car")
public class CarController {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private RegionRepository regionRepository;

    @PostMapping
    public String add(@RequestBody CarDto carDto){
        Optional<Users> optionalUsers = usersRepository.findById(carDto.getUserId());
        if(!optionalUsers.isPresent())
            return "user topilmadi";

        Optional<Region> optionalRegion = regionRepository.findById(carDto.getRegionId());
        if(!optionalRegion.isPresent())
            return "region topilmadi";

        Car car = new Car();
        car.setModel(carDto.getModel());
        car.setRegion(optionalRegion.get());
        car.setMadeYear(carDto.getMadeYear());
        car.setStateNumber(carDto.getStateNumber());
        car.setType(carDto.getType());
        Car savedCar = carRepository.save(car);
        Users user = optionalUsers.get();
        user.getCars().add(savedCar);
        usersRepository.save(user);
        return savedCar.toString();
    }

    @GetMapping("/getByUserId/{userId}")
    public List<Car> getByUserId(@PathVariable Integer userId) {
        return carRepository.getAllUserCars(userId);
    }

    @GetMapping("/getByRegionId/{regId}")
    public List<Car> getByRegionId(@PathVariable Integer regId) {
        return carRepository.findAllByRegion_Id(regId);
    }
}
