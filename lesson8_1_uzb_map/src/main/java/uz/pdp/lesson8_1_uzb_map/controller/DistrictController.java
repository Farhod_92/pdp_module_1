package uz.pdp.lesson8_1_uzb_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson8_1_uzb_map.entity.District;
import uz.pdp.lesson8_1_uzb_map.entity.Region;
import uz.pdp.lesson8_1_uzb_map.payload.DistrictDto;
import uz.pdp.lesson8_1_uzb_map.repository.DistrictRepository;
import uz.pdp.lesson8_1_uzb_map.repository.RegionRepository;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/district")
public class DistrictController {

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private RegionRepository regionRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody DistrictDto districtDto){
        Optional<Region> optionalRegion = regionRepository.findById(districtDto.getRegionId());
        if(!optionalRegion.isPresent())
            return "region topilmadi";

        if(districtRepository.existsByNameAndRegion_Id(districtDto.getName(), districtDto.getRegionId()))
            return "bu regionda bunaqa tuman mavjud!";

        District district = new District();
        district.setName(districtDto.getName());
        district.setRegion(optionalRegion.get());
        District savedDistrict = districtRepository.save(district);
        return "district saqlandi:" + savedDistrict.toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public District getById(@PathVariable Integer id){
        Optional<District> optionalDistrict= districtRepository.findById(id);
        return optionalDistrict.orElse(null);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<District> getAll(){
        return districtRepository.findAll();
    }

    @GetMapping("/getAllByRegionId/{regionId}")
    public List<District> getAllByRegionId(@PathVariable Integer regionId){
        List<District> allByRegion_id = districtRepository.findAllByRegion_Id(regionId);
        List<District> districtsByRegionId = districtRepository.getDistrictsByRegionId(regionId);
        List<District> districtsByRegionIdNative = districtRepository.getDistrictsByRegionIdNative(regionId);
        return districtsByRegionIdNative;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody DistrictDto districtDto){
        Optional<Region> optionalRegion = regionRepository.findById(districtDto.getRegionId());
        if(!optionalRegion.isPresent())
            return "region topilmadi";

        Optional<District> optionalDistrict = districtRepository.findById(id);
        if(!optionalDistrict.isPresent())
            return "district topilmadi";

        if(districtRepository.existsByNameAndRegion_Id(districtDto.getName(), districtDto.getRegionId()))
            return "bu regionda bunaqa tuman mavjud!";

        District editingDistrict = optionalDistrict.get();
        editingDistrict.setName(districtDto.getName());
        editingDistrict.setRegion(optionalRegion.get());
        districtRepository.save(editingDistrict);
        return "district o'zgartirildi:" +  editingDistrict.toString();
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String deleteContinent(@PathVariable Integer id){
        try {
            districtRepository.deleteById(id);
            return "district o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }

}
