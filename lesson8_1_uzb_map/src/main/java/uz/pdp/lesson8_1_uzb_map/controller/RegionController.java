package uz.pdp.lesson8_1_uzb_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson8_1_uzb_map.entity.Region;
import uz.pdp.lesson8_1_uzb_map.repository.RegionRepository;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/region")
public class RegionController {

    @Autowired
    private RegionRepository regionRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String save(@RequestBody Region region){
       if(regionRepository.existsByName(region.getName()))
           return "bunaqa region mavjud";
        Region region1 = regionRepository.save(region);
        return "region saqlandi:" + region1.toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public Region getById(@PathVariable Integer id){
        Optional<Region> optionalRegion = regionRepository.findById(id);
        return optionalRegion.orElse(null);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Region> getAll(){
        return regionRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public String edit(@PathVariable Integer id, @RequestBody Region region){
        Optional<Region> optionalRegion = regionRepository.findById(id);
        if(!optionalRegion.isPresent())
            return "region topilmadi";

        Region editingRegion =optionalRegion.get();
        editingRegion.setName(region.getName());
        regionRepository.save(editingRegion);
        return "region o'zgartirildi:" +  editingRegion.toString();
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public String deleteContinent(@PathVariable Integer id){
        try {
            regionRepository.deleteById(id);
            return "region o'chirildi";
        }catch (Exception e){
            return "xatolik: " + e.getMessage();
        }
    }

}
