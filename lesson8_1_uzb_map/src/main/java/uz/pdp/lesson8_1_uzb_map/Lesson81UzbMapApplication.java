package uz.pdp.lesson8_1_uzb_map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson81UzbMapApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson81UzbMapApplication.class, args);
    }

}
