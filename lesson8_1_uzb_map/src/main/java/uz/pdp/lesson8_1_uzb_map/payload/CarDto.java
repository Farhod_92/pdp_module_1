package uz.pdp.lesson8_1_uzb_map.payload;

import lombok.Getter;

@Getter
public class CarDto {
    private String model;

    private String stateNumber;

    private String madeYear;

    private String type;

    private Integer regionId;

    private Integer userId;
}
