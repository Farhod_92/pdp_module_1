package uz.pdp.lesson8_1_uzb_map.payload;

import lombok.Getter;

@Getter
public class AddressDto {
    private String street;
    private String homeNumber;
    private int districtId;
}
