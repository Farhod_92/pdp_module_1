package uz.pdp.lesson8_1_uzb_map.payload;

import lombok.Getter;

@Getter
public class DistrictDto {
    private String name;
    private int regionId;
}
