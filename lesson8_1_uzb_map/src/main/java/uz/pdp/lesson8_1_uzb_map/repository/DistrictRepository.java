package uz.pdp.lesson8_1_uzb_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.lesson8_1_uzb_map.entity.District;

import java.util.List;


public interface DistrictRepository extends JpaRepository<District, Integer> {

    boolean existsByNameAndRegion_Id(String name, int region_id);

    List<District> findAllByRegion_Id(Integer regionId);

    // @Query("select gr from groups gr where gr.faculty.university.id=?1")
    @Query("select d from  District d where d.region.id=:regionId")
    List<District> getDistrictsByRegionId(Integer regionId);

    @Query(value = "select * from district d where d.region_id=:regionId", nativeQuery = true)
    List<District> getDistrictsByRegionIdNative(Integer regionId);
}
