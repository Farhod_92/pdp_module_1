package uz.pdp.lesson8_1_uzb_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson8_1_uzb_map.entity.Region;

public interface RegionRepository extends JpaRepository<Region, Integer> {
    boolean existsByName(String name);
}
