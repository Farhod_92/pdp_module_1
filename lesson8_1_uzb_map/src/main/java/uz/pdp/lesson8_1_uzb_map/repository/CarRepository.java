package uz.pdp.lesson8_1_uzb_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.lesson8_1_uzb_map.entity.Car;

import java.util.List;


public interface CarRepository extends JpaRepository<Car, Integer> {

    List<Car> findAllByRegion_Id(int region_id);

    @Query(value = "select c.id, c.made_year, c.model, c.state_number, c.type, c.region_id from users u\n" +
            "    join users_cars uc on u.id = uc.users_id\n" +
            "    join car c on uc.cars_id = c.id where u.id=:userId", nativeQuery = true)
    List<Car> getAllUserCars(Integer userId);


}
