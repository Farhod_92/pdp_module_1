package uz.pdp.lesson8_1_uzb_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.lesson8_1_uzb_map.entity.Address;

import java.util.List;


public interface AddressRepository extends JpaRepository<Address, Integer> {
    List<Address> findAllByDistrict_id(int district_id);

    @Query("select a from Address a where a.district.id=:district_id")
    List<Address> getAllByDistrictId(int district_id);

    @Query(value = "select * from address a join district d on a.district_id=d.id where d.id=:district_id", nativeQuery = true)
    List<Address> getAllByDistrictIdNative(int district_id);
}
