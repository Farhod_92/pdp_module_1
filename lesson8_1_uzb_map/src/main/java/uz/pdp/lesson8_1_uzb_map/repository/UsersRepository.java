package uz.pdp.lesson8_1_uzb_map.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.lesson8_1_uzb_map.entity.Car;
import uz.pdp.lesson8_1_uzb_map.entity.Users;

import java.util.List;


public interface UsersRepository extends JpaRepository<Users, Integer> {
    @Query(value = "select * from  auto_shop a join auto_shop_cars ca on ca.auto_shop_id=a.id join car c on ca.cars_id=c.id where a.id=:shopId ", nativeQuery = true)
    List<Car> getAllShopCars(Integer shopId);
}
