package uz.pdp.lesson_9_file_upload_db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson9FileUploadDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson9FileUploadDbApplication.class, args);
    }

}
