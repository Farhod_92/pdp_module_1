package uz.pdp.lesson_9_file_upload_db.repostitory;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson_9_file_upload_db.entity.AttachmentContent;

import java.util.Optional;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, Integer> {
    Optional<AttachmentContent> findByAttachmentId(Integer attachment_id);
}
