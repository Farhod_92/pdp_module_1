package uz.pdp.lesson_9_file_upload_db.repostitory;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson_9_file_upload_db.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {
}
