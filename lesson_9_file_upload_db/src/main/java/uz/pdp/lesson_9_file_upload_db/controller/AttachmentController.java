package uz.pdp.lesson_9_file_upload_db.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.lesson_9_file_upload_db.entity.Attachment;
import uz.pdp.lesson_9_file_upload_db.entity.AttachmentContent;
import uz.pdp.lesson_9_file_upload_db.repostitory.AttachmentContentRepository;
import uz.pdp.lesson_9_file_upload_db.repostitory.AttachmentRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Optional;

@RestController
@RequestMapping("/attachment")
public class AttachmentController {
    @Autowired
    private AttachmentRepository attachmentRepository;

    @Autowired
    private AttachmentContentRepository attachmentContentRepository;

    @PostMapping("/upload")
    public String uploadFile(MultipartHttpServletRequest request) throws IOException {
        long l = System.currentTimeMillis();
        //ATTACHMENT
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());
        if(file != null) {
            String originalFilename = file.getOriginalFilename();
            long size = file.getSize();
            String contentType = file.getContentType();

            Attachment attachment = new Attachment();
            attachment.setFileOriginalName(originalFilename);
            attachment.setSize(size);
            attachment.setContentType(contentType);
            Attachment savedAttachment = attachmentRepository.save(attachment);

            //ATTACHMENT CONTENT
            AttachmentContent attachmentContent =new AttachmentContent();
            attachmentContent.setContent(file.getBytes());
            attachmentContent.setAttachment(savedAttachment);
            attachmentContentRepository.save(attachmentContent);
            System.out.println(System.currentTimeMillis()-l);
            return "fayl saqlandi. ID=" + savedAttachment.getId();
        }
        return "Xatolik";
    }

    @GetMapping("/download/{attachmentId}")
    public void download(@PathVariable Integer attachmentId, HttpServletResponse response) throws IOException {
        long l = System.currentTimeMillis();
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(attachmentId);
        if(optionalAttachment.isPresent()){
            Attachment attachment = optionalAttachment.get();

            Optional<AttachmentContent> optionalAttachmentContent = attachmentContentRepository.findByAttachmentId(attachmentId);
            if(optionalAttachmentContent.isPresent()){
                AttachmentContent attachmentContent = optionalAttachmentContent.get();
                response.setHeader("Content-Disposition", "inline"); // FAQAT KO'RSATISH
//                //response.setHeader("Content-Disposition", "attachment"); // DOWNLOAD
//                response.setHeader("Content-Disposition", "attachment; filename=\""+attachment.getFileOriginalName()+"\""); //DOWNLOAD
                response.setContentType(attachment.getContentType());
                FileCopyUtils.copy(attachmentContent.getContent(), response.getOutputStream());
            }
        }
        System.out.println(System.currentTimeMillis()-l);
    }
}
