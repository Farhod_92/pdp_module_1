
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student page</title>
</head>
<body>
<h1>Student page</h1>
<button><a href="/addStudent">Add student</a></button>
<br>
<c:forEach items="${students}" var="student">
        <tr><b>Full name:</b> <c:out value="${student.fullName}"/></tr>
        <tr><b>Phone number:</b> <c:out value="${student.phoneNumber}"/></tr>
        <tr><b>Age:</b> <c:out value="${student.age}"/></tr>
    <br>
</c:forEach>

<h1>${response}</h1>

</body>
</html>
