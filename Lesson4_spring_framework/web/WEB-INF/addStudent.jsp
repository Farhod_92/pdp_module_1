
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Student </title>
</head>
<body>
<h1> Add Student page</h1>
<form action="/addStudent" method="post">
    <input type="text" name="fullName" placeholder="Full name">
    <input type="text" name="age" placeholder="Age">
    <input type="text" name="phoneNumber" placeholder="Phone number">
    <button type="submit">Add</button>
</form>
</body>
</html>
