package uz.pdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.model.Student;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class StudentController {
    List<Student> students =  new ArrayList(
        Arrays.asList(
                new Student("Tohir", 36, "12345678"),
                new Student("Abbos", 21, "12345678"),
                new Student("Javohir", 32, "12345678"),
                new Student("Sobir", 434, "12345678")
        )
    );


    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public String student(Model model){
        model.addAttribute("students",students);
        return "student";
    }


    @RequestMapping(value = "/addStudent", method = RequestMethod.GET)
    public String addStudentPage(){

        return "addStudent";
    }

    @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
    public String addStudent(@RequestParam String fullName, @RequestParam String phoneNumber, @RequestParam Integer age, Model model){
        System.out.println(fullName);
        students.add(new Student(fullName, age, phoneNumber));
        model.addAttribute("response", "Succesfully ");
        return "redirect:/student";
    }
}
