package uz.pdp.service;

import uz.pdp.model.User;

import java.sql.*;

public class DbService {
    private static String url = "jdbc:postgresql://localhost:5432/auth";
    private static String username = "postgres";
    private static String password = "123";


    public boolean login(String username, String password){
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
            String query = "select count(*) from users where username=? and password=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            int count=0;
            while (resultSet.next()){
                count= resultSet.getInt(1);
            }
            preparedStatement.close();
            connection.close();
            if(count>0)
                return true;
            else
                return false;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    public String registerUser(User user){
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection =  DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            String checkUsernameQuery = "select count(*) from users where username='" + user.getUsername() + "'";
            ResultSet resultSet = statement.executeQuery(checkUsernameQuery);

            int count = 0;
            while(resultSet.next()){
                count = resultSet.getInt(1);
            }

            if(count > 0)
                return "Username exists";

            String query = "insert into users(username, name, password) values(?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.execute();
            return "User successfully added";
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return throwables.getMessage();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }


    private Connection getConnection(){
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
