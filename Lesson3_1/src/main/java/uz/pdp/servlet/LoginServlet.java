package uz.pdp.servlet;

import uz.pdp.model.User;
import uz.pdp.service.DbService;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
      resp.sendRedirect("log.html");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        request.getRequestDispatcher("link.html").include(request, response);

        String username=request.getParameter("username");
        String name=request.getParameter("name");
        String password=request.getParameter("password");
        String prePassword=request.getParameter("prePassword");

        if(password.length()>7 && password.equals(prePassword)){
            HttpSession session=request.getSession();
            session.setAttribute("name",name);
            DbService dbService = new DbService();
            String result = dbService.registerUser(new User(username, name, password));
            out.print(result);
           // response.sendRedirect("/login");
        }
        else{
            out.print("Sorry, wrong password");
            request.getRequestDispatcher("login.html").include(request, response);
        }
        out.close();
    }
}