package uz.pdp.servlet;

import uz.pdp.service.DbService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/log")
public class LogServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        DbService dbService= new DbService();
        PrintWriter writer = resp.getWriter();


        if(dbService.login(username, password)) {
            HttpSession session=request.getSession();
            session.setAttribute("name",username);
            resp.sendRedirect("/cabinet");
        }
        else
            writer.write("Bunday user mavjud emas!");
    }
}
