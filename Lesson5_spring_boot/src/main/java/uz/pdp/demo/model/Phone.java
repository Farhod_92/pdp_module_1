package uz.pdp.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Phone {
    private Integer id;
    private String model;
    private String macAddress;
    private Integer phoneNumber;
}
