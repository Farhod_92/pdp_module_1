package uz.pdp.demo.controller;

import org.springframework.web.bind.annotation.*;
import uz.pdp.demo.model.Student;

import java.util.*;

@RestController
public class PersonController {
    List<Student> students = new ArrayList<>(Arrays.asList(
            new Student(1,"Nozim", "1111111111111", "java"),
            new Student(2,"Tohir", "22222222222", "python"),
            new Student(3,"Rahim", "3333333333", "django"),
            new Student(4,"Tesha", "4444444444", "flutter"),
            new Student(5,"Po'lot", "5555555555", "javascript")
    ));

    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public List<Student> getStudents(){
        return students;
    }

    @RequestMapping(value = "/student/{id}", method = RequestMethod.GET)
    public Student getStudentById(@PathVariable Integer id){
        for (Student student : students) {
            if(student.getId().equals(id))
                return student;
        }
        return null;
    }

    @RequestMapping(value = "/student", method = RequestMethod.POST)
    public String addStudent(@RequestBody Student student){
        for (Student s : students) {
            if(s.getPhoneNumber().equals(student.getPhoneNumber()))
                return "Boshqa tel. raqam kiritilsin";
        }

        Integer id = students.get(students.size() - 1).getId();
        student.setId(id+1);
        students.add(student);
        return "student qo'shildi";
    }

        @RequestMapping(value = "/student/{id}", method = RequestMethod.PUT)
    public String editStudentById(@PathVariable Integer id, @RequestBody Student student){

        for (Student item : students) {
            if(item.getId() == id){
                item.setName(student.getName());
                item.setCourseName(student.getCourseName());
                item.setPhoneNumber(student.getPhoneNumber());
                return "o'zgartirildi";
            }
        }
        return "bunday student mavjud emas";
    }

    @RequestMapping(value = "/student/{id}", method = RequestMethod.DELETE)
    public String deleteStudentById(@PathVariable Integer id){
        for (Student student : students) {
            if(student.getId().equals(id)) {
                students.remove(student);
                return "o'chirildi";
            }
        }

        return "student topilmadi";
    }


}
