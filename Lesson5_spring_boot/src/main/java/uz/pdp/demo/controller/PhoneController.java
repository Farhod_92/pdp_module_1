package uz.pdp.demo.controller;

import org.springframework.web.bind.annotation.*;
import uz.pdp.demo.model.Phone;
import uz.pdp.demo.model.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class PhoneController {
    List<Phone> phoneList = new ArrayList<>(Arrays.asList(
            new Phone(1, "S20", "123:44", 12345),
            new Phone(2, "A51", "3232:43234", 111),
            new Phone(3, "13 pro", "6543:443242", 222),
            new Phone(4, "10S", "321:4445543", 333)
    ));

    @RequestMapping(value = "/phone", method = RequestMethod.GET)
    public List<Phone> getStudents(){
        return phoneList;
    }

    @RequestMapping(value = "/phone/{id}", method = RequestMethod.GET)
    public Phone getStudentById(@PathVariable Integer id){
        for (Phone phone : phoneList) {
            if(phone.getId().equals(id))
                return phone;
        }
        return null;
    }

    @RequestMapping(value = "/phone", method = RequestMethod.POST)
    public String addStudent(@RequestBody Phone phone){
        for (Phone p : phoneList) {
            if(p.getMacAddress().equals(phone.getMacAddress()))
                return "Boshqa mac address kiritilsin";
        }

        Integer id = phoneList.get(phoneList.size() - 1).getId();
        phone.setId(id+1);
        phoneList.add(phone);
        return "Telefon qo'shildi";
    }

    @RequestMapping(value = "/phone/{id}", method = RequestMethod.PUT)
    public String editStudentById(@PathVariable Integer id, @RequestBody Phone phone){

        for (Phone item : phoneList) {
            if(item.getId() == id){
                item.setMacAddress(phone.getMacAddress());
                item.setModel(phone.getModel());
                item.setPhoneNumber(phone.getPhoneNumber());
                return "o'zgartirildi";
            }
        }
        return "bunday telefon mavjud emas";
    }

    @RequestMapping(value = "/phone/{id}", method = RequestMethod.DELETE)
    public String deleteStudentById(@PathVariable Integer id){
        for (Phone phone : phoneList) {
            if(phone.getId().equals(id)) {
                phoneList.remove(phone);
                return "o'chirildi";
            }
        }

        return "telefon topilmadi";
    }


}
